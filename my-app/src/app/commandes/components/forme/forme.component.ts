import { Component, OnInit } from '@angular/core';
import { State } from '../../enums/state.enum';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { COLLECTION } from '../../collection';

@Component({
  selector: 'app-forme',
  templateUrl: './forme.component.html',
  styleUrls: ['./forme.component.scss']
})
export class FormeComponent implements OnInit {

  collection = COLLECTION;
  state = State;
  form: FormGroup;
  nameCtrl: FormControl;
  refCtrl: FormControl;
  stateCtrl: FormControl;

  constructor(fb: FormBuilder) {
    this.nameCtrl = fb.control('', [
      Validators.required,
      Validators.minLength(5)
    ]);
    this.refCtrl = fb.control('', [
      Validators.required,
      Validators.minLength(4)
    ]);
    this.stateCtrl = fb.control(this.state.ALIVRER);
    // initiliser
    this.form = fb.group({
      name : this.nameCtrl,
      ref : this.refCtrl,
      state : this.stateCtrl
    });
  }
  addCmd() {
    this.collection.push({
      name: this.form.get('name').value,
      reference: this.form.get('ref').value,
      state: this.form.get('state').value
    });
    this.reset();
  }
  reset() {
    this.form.reset();
    this.form.get('state').setValue(this.state.ALIVRER);
  }

  ngOnInit() {
  }

}



