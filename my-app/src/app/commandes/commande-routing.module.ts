import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';
import { ListCommandesComponent } from './containers/list-commandes/list-commandes.component';
import { FormeComponent } from './components/forme/forme.component';

const cmdRoutes: Routes = [
  { path: 'list',   component: ListCommandesComponent },
  { path: 'add',   component: FormeComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      cmdRoutes
    )
  ],
  declarations: []
})
export class CommandeRoutingModule { }
