import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { ListCommandesComponent } from './containers/list-commandes/list-commandes.component';
import { CommandeComponent } from './components/commande/commande.component';
import { CommandeRoutingModule } from './commande-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormeComponent } from './components/forme/forme.component';



@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CommandeRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    ListCommandesComponent,
    CommandeComponent,
    FormeComponent
  ],
  exports: [
    ListCommandesComponent
  ]
})
export class CommandesModule { }
