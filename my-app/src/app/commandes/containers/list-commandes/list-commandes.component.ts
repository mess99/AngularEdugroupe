import { Component, OnInit } from '@angular/core';
import { Commande } from '../../interfaces/commande.model';
import { COLLECTION } from '../../collection';


@Component({
  selector: 'app-list-commandes',
  templateUrl: './list-commandes.component.html',
  styleUrls: ['./list-commandes.component.scss']
})
export class ListCommandesComponent implements OnInit {
  collection: Commande[] = COLLECTION;
  constructor() { }

  ngOnInit() {
  }

}
