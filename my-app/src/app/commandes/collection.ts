import { Commande } from './interfaces/commande.model';

export const COLLECTION: Commande[] = [
  {
    name: 'mess BENCH',
    reference: '1234',
    state: 0
  },
  {
    name: 'chou BENb',
    reference: '5522',
    state: 1
  },
  {
    name: 'bouou BELL',
    reference: '1265',
    state: 2
  }
];
