import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { PageNotFondComponent } from './components/page-not-fond/page-not-fond.component';
import { HomeComponent } from './components/home/home.component';
import { CoreRoutingModule } from './core-routing.module';


@NgModule({
  imports: [
    CommonModule,
    CoreRoutingModule
  ],
  declarations: [
    HomeComponent,
    PageNotFondComponent
  ],
  exports: [
    HomeComponent,
    PageNotFondComponent
  ]
})
export class CoreModule { }
