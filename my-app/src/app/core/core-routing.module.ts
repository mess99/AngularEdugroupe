import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { PageNotFondComponent } from './components/page-not-fond/page-not-fond.component';

const coreRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: '**', component: PageNotFondComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      coreRoutes
    )
  ],
  declarations: []
})
export class CoreRoutingModule { }
